export const light = {
    theme: {
      background: '#ededed',
      backgroundNative: '#171717',
      paper: '#e6e6e6',
      paperNative: '#202124',
      border: '#bdbdbd', 
      text: '#696969'
    }
}

export const dark = {
    theme: {
      background: '#171717',
      backgroundNative: '#ededed',
      paper: '#202124',
      paperNative: '#e6e6e6',
      border: '#575c66', 
      text: '#ececec'
    }
}
