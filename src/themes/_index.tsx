import React, { createContext, useState, useEffect } from 'react'
import { ThemeProvider } from 'styled-components'

import {light} from './_style'
import {dark} from './_style'  

const ThemeContext = createContext({
    mode: 'dark',
    setMode: (mode: string) => console.log(mode)
})

export const useTheme = () => React.useContext(ThemeContext)

//@ts-ignore
const ManageThemeProvider = ({ children }) => {
    const [themeState, setThemeState] = useState('dark')
    const setMode = (mode: string)=> {
      setThemeState(mode)
    } 
    useEffect(()=>{
      //set local last default color
    },[])
    return (
      <ThemeContext.Provider value={{ mode: themeState, setMode }}>
        <ThemeProvider theme={themeState === 'dark' ? dark.theme : light.theme}>
          {children}
        </ThemeProvider>
      </ThemeContext.Provider>
    )
}

//@ts-ignore
const ThemeManager = ({ children }) => (<ManageThemeProvider>{children}</ManageThemeProvider>)
  
export default ThemeManager