import { configureStore } from '@reduxjs/toolkit'
import locationSlice from '../reducers/location'
import searchSlice from '../reducers/search'
import favoritesSlice from '../reducers/favorites'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas/_index'

const sagaMiddleware = createSagaMiddleware()
const store = configureStore({
  reducer: {
    location: locationSlice,
    favorites: favoritesSlice,
    search: searchSlice
  },
  middleware: (getDefaultMiddleware) => [...getDefaultMiddleware({ thunk: false }), sagaMiddleware]
})
sagaMiddleware.run(rootSaga)

export type RootState = ReturnType<typeof store.getState> 
export type AppDispatch = typeof store.dispatch

export default store;
