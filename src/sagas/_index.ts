import { call, put, takeLatest } from 'redux-saga/effects'
import { setCurrentFavorite, setFavorites } from '../reducers/favorites'
import { setLocationPosition, setLocationWeather, setLocation5DaysWeather } from '../reducers/location'
import { setOffmode, setSearchData } from '../reducers/search'
import { appServices, messageService } from '../services/_index'

const appkey = 'oer1rTGI1wvBDA1NTaAHOhp082RhHsU5' //YdzWlP2sTXuJSltQSIFHI4bmPh3wa0Ai gALGorxSHUlvlXlyd7CnNrqLBEi0CiC2 oer1rTGI1wvBDA1NTaAHOhp082RhHsU5
const appfirebasekey = 'presolu-67a8a'

function* fetchCurrentFavorite(action: any) { 
    const {favorite} = action
    try { 
        yield put(setLocationWeather({
            weather: favorite['iconPhrase'],
            weatherIcon: favorite['icon'],
            isDayTime: favorite['isDayTime'] === 'null' ? (new Date()).getHours() > 17 ? false : true : false,
            temperatureMetricC: favorite['temperatureMetricC'],
            temperatureImperialF: favorite['temperatureImperialF'],
            city: favorite['city'],
            date: favorite['date']
        }));
        let days = []
        for(let i = 0; i < Object.keys(favorite['days']).length;i++)
            days.push({
                Date: favorite['days'][i + 1]['mapValue']['fields']['Date']['stringValue'],
                Temperature: {Maximum: {Value: parseInt(favorite['days'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Maximum']['mapValue']['fields']['Value']['integerValue']), Unit: favorite['days'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Maximum']['mapValue']['fields']['Unit']['stringValue']}, Minimum: {Value: favorite['days'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Minimum']['mapValue']['fields']['Value']['integerValue'], Unit: favorite['days'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Minimum']['mapValue']['fields']['Unit']['stringValue']}},
                Day: {Icon: parseInt(favorite['days'][i + 1]['mapValue']['fields']['DayIcon']['integerValue']), IconPhrase: favorite['days'][i + 1]['mapValue']['fields']['DayIconPhrase']['stringValue']},
                Night: {Icon: parseInt(favorite['days'][i + 1]['mapValue']['fields']['NightIcon']['integerValue']), IconPhrase: favorite['days'][i + 1]['mapValue']['fields']['NightIconPhrase']['stringValue']},
            })
        yield put(setLocation5DaysWeather({
                days: days
            }));
        yield put(setCurrentFavorite({
                favorite: favorite['city']
            }));
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchFavorites(action: any) { 
    try { 
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://firestore.googleapis.com/v1/projects/${appfirebasekey}/databases/(default)/documents/herolo/favorites`}));
        let favorites = []
        let favorite = null
        for (const key in result['fields']) { 
            if(result['fields'][key]['mapValue']['fields']['City']['stringValue'] === action.city)
                favorite = action.city
            favorites.push({
                city: result['fields'][key]['mapValue']['fields']['City']['stringValue'],
                date: result['fields'][key]['mapValue']['fields']['Date']['stringValue'],
                days: result['fields'][key]['mapValue']['fields']['Days']['mapValue']['fields'],
                icon: result['fields'][key]['mapValue']['fields']['Icon']['integerValue'],
                iconPhrase: result['fields'][key]['mapValue']['fields']['IconPhrase']['stringValue'],
                locationKey: result['fields'][key]['mapValue']['fields']['LocationKey']['stringValue'],
                temperatureMetricC: result['fields'][key]['mapValue']['fields']['Temperature']['mapValue']['fields']['MetricC']['integerValue'],
                temperatureImperialF: result['fields'][key]['mapValue']['fields']['Temperature']['mapValue']['fields']['ImperialF']['integerValue'],
                isDayTime: result['fields'][key]['mapValue']['fields']['isDayTime']['stringValue']
            })
        } 
        favorites = favorites.sort((favA,favB)=>{
            return  ('' + favA['city']).localeCompare(favB['city']);
        })
        yield put(setFavorites({
                favorites: favorites
            }));
        if(favorite)
            yield put(setCurrentFavorite({
                    favorite: favorite
                }));
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchAddFavorite(action: any) {
    const {firebaseData, locationKey, days} = action; 
    const {city, metricC, imperialF, icon, iconPhrase, isDayTime} = firebaseData;
    try { 
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://firestore.googleapis.com/v1/projects/${appfirebasekey}/databases/(default)/documents/herolo/favorites`}));
        const body = appServices.getFirebaseJsonAsString({locationKey: locationKey, isDayTime: isDayTime, city: city, metricC: metricC, imperialF: imperialF, icon: icon, iconPhrase: iconPhrase, days: days}, true, JSON.stringify(result['fields']))
        yield call(() => appServices.callAPI({ url: `https://firestore.googleapis.com/v1/projects/${appfirebasekey}/databases/(default)/documents/herolo/favorites`, method: 'PATCH', body: body}));
        yield put(setCurrentFavorite({
                favorite: city
            }));
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchDataOnFirebase(firebaseData: {city: string, metricC: number, imperialF: number, icon: number, iconPhrase: string, isDayTime: boolean}, days: [], locationKey: string) {
    const {city, metricC, imperialF, icon, iconPhrase, isDayTime} = firebaseData;
    try { 
        const body = appServices.getFirebaseJsonAsString({locationKey: locationKey, isDayTime: isDayTime, city: city, metricC: metricC, imperialF: imperialF, icon: icon, iconPhrase: iconPhrase, days: days})
        //@ts-ignore
        yield call(() => appServices.callAPI({ url: `https://firestore.googleapis.com/v1/projects/${appfirebasekey}/databases/(default)/documents/herolo/weather`, method: 'PATCH', body: body}));
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchFirebaseData() {
    try { 
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://firestore.googleapis.com/v1/projects/${appfirebasekey}/databases/(default)/documents/herolo/weather`}));
        yield put(setLocationWeather({
            weather: result['fields']['IconPhrase']['stringValue'],
            weatherIcon: result['fields']['Icon']['integerValue'],
            isDayTime: result['fields']['isDayTime']['stringValue'] === 'null' ? (new Date()).getHours() > 17 ? false : true : false,
            temperatureMetricC:result['fields']['Temperature']['mapValue']['fields']['MetricC']['integerValue'],
            temperatureImperialF: result['fields']['Temperature']['mapValue']['fields']['ImperialF']['integerValue'],
            city: result['fields']['City']['stringValue'],
            date: result['fields']['Date']['stringValue']
        }));
        let days = []
        for(let i = 0; i < Object.keys(result['fields']['Days']['mapValue']['fields']).length;i++)
            days.push({
                Date: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['Date']['stringValue'],
                Temperature: {Maximum: {Value: parseInt(result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Maximum']['mapValue']['fields']['Value']['integerValue']), Unit: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Maximum']['mapValue']['fields']['Unit']['stringValue']}, Minimum: {Value: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Minimum']['mapValue']['fields']['Value']['integerValue'], Unit: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['Temperature']['mapValue']['fields']['Minimum']['mapValue']['fields']['Unit']['stringValue']}},
                Day: {Icon: parseInt(result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['DayIcon']['integerValue']), IconPhrase: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['DayIconPhrase']['stringValue']},
                Night: {Icon: parseInt(result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['NightIcon']['integerValue']), IconPhrase: result['fields']['Days']['mapValue']['fields'][i + 1]['mapValue']['fields']['NightIconPhrase']['stringValue']},
            })
        yield put(setLocation5DaysWeather({
                days: days
            }));
        yield put(setOffmode({
                offmode: true
            }));
        yield fetchFavorites({city: result['fields']['City']['stringValue']})
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchLocation5DaysWeather(locationKey: string, firebaseData: {city: string, metricC: number, imperialF: number, icon: number, iconPhrase: string, isDayTime: boolean}) {
    try { 
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://dataservice.accuweather.com/forecasts/v1/daily/5day/${locationKey}?apikey=${appkey}`}));
        const days = result && result['DailyForecasts'] ? result['DailyForecasts'] : null 
        // console.debug('fetchLocation5DaysWeather',result)
        yield put(setLocation5DaysWeather({
                days: days
            }));
        yield fetchDataOnFirebase(firebaseData, days, locationKey)
    } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000)
    }
}

function* fetchLocationWeather(action: any) {
    try { 
        const locationKey = action && action['locationKey'] ? action['locationKey'] : action
        const city = action && action['label'] ? action['label'] : action && action['city'] ? action['city'] : null
        // console.debug('fetchLocationWeather',action,city)
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://dataservice.accuweather.com/currentconditions/v1/${locationKey}?apikey=${appkey}`}));
        const weather = result && result[0]['WeatherText'] ? result[0]['WeatherText'] : null
        const weatherIcon = result && result[0]['WeatherIcon'] ? result[0]['WeatherIcon'] : null 
        const isDayTime = result && result[0]['IsDayTime'] ? result[0]['IsDayTime'] : null 
        const temperatureMetricC = result && result[0]['Temperature'] ? result[0]['Temperature']['Metric']['Value'] : null 
        const temperatureImperialF = result && result[0]['Temperature'] ? result[0]['Temperature']['Imperial']['Value'] : null 
        // console.debug('fetchLocationWeather',result)
        const firebaseData = {
            isDayTime: isDayTime, city: city ? city : 'Tel Aviv', metricC: temperatureMetricC, imperialF: temperatureImperialF, icon: weatherIcon, iconPhrase: weather
        }
        if(result)
            yield fetchLocation5DaysWeather(locationKey, firebaseData)
        yield put(setLocationWeather({
            weather: weather,
            weatherIcon: weatherIcon,
            isDayTime: isDayTime,
            temperatureMetricC: temperatureMetricC,
            temperatureImperialF: temperatureImperialF,
            city: city
        }));
    } catch (e: any) { 
        messageService.handleMsg('it`s look like the Weather API doesn`t work right now,\nyou can watch last firebase update!!', 7000) 
        yield fetchFirebaseData()
    }
}

function* fetchUserLocation(action: any) {
   try { 
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=${appkey}&q=${action.location.latitude}%2C${action.location.longitude}`}));
        const locationKey = result && result['Key'] ? result['Key'] : null
        const city = result && result['EnglishName'] ? result['EnglishName'] : null  
        // console.debug('fetchUserLocation',result, city)
        if(result)
            yield fetchLocationWeather({locationKey, city})
        yield put(setLocationPosition({
            longitude: action.location.longitude,
            latitude: action.location.latitude,
            locationKey: locationKey,
            city: city
        }));
        yield fetchFavorites({city: city})
   } catch (e: any) {
        messageService.handleMsg('it`s look like the Weather API doesn`t work right now,\nyou can watch last firebase update!!', 7000) 
        yield fetchFirebaseData()
   }
}

function* fetchSearch(action: any) {
   try {  
        //@ts-ignore
        const result = yield call(() => appServices.callAPI({ url: `https://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=${appkey}&q=${action.label}`}))
        // console.log('fetchSearch',result) 
        const data: object[] = []
        if(result)
            result.forEach((row: any)=>{
                data.push({locationKey: row['Key'], value: `${row['LocalizedName']}`.toLowerCase(), label: `${row['LocalizedName']}`})
            })
        yield put(setSearchData({
                data: data
            }));
   } catch (e: any) {
        messageService.handleMsg(`${e.toString()}`, 7000) 
   }
}

export default function* rootSaga() {
    yield takeLatest("FETCH_USER_LOCATION", fetchUserLocation)
    yield takeLatest("FETCH_LOCATION_WEATHER", fetchLocationWeather)
    yield takeLatest("FETCH_SEARCH", fetchSearch)
    yield takeLatest("FETCH_ADD_FAVORITE", fetchAddFavorite)
    yield takeLatest("FETCH_FAVORITES", fetchFavorites)
    yield takeLatest("FETCH_CURRENT_FAVORITE", fetchCurrentFavorite)
  }