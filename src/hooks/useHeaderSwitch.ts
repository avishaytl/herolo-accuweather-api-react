import { useCallback, useEffect, useState } from "react"

const useHeaderSwitch = (tabs: string[], index?: number):[number,(index: number, preIndex: number)=>void] =>{
    const [switchPosition,setSwitchPosition] = useState(index ? index : 0)
    const actionSwitch = useCallback((index: number, preIndex: number) =>{
        for(let i = 0;i < tabs.length;i++){ 
            const elePage = document.getElementById(`${tabs[i].toLowerCase()}-page`)
            if(elePage){
                elePage.classList.remove("page-visible")
                elePage.classList.add("page-hidden")
            }
        }
        const elePage = document.getElementById(`${tabs[index].toLowerCase()}-page`)
        const eleSwitchOne = document.getElementById(`${tabs[0].toLowerCase()}-switch`)
        const eleSwitchTwo = document.getElementById(`${tabs[1].toLowerCase()}-switch`)
        if(elePage){
            elePage.classList.remove("page-hidden")
            elePage.classList.add("page-visible")
        }
        if(eleSwitchOne){
            if(!index){
                eleSwitchOne.classList.remove("switch-hidden")
                eleSwitchOne.classList.add("switch-visible") 
            }else{
                eleSwitchOne.classList.remove("switch-visible")
                eleSwitchOne.classList.add("switch-hidden")  
            }
        }
        if(eleSwitchTwo){
            if(index){
                eleSwitchTwo.classList.remove("switch-hidden")
                eleSwitchTwo.classList.add("switch-visible") 
            }else{
                eleSwitchTwo.classList.remove("switch-visible")
                eleSwitchTwo.classList.add("switch-hidden")  
            }
        }
        setSwitchPosition(index)
    },[])
    useEffect(()=>{
        if(index)
            actionSwitch(index, 0)
    },[])
    return [switchPosition,actionSwitch]
}
export default useHeaderSwitch