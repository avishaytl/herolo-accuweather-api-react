interface LocationServicesType{ 
    getCurrentPosition(dispatch: any):void
}

function _locationServices():LocationServicesType{
    return({
        getCurrentPosition(dispatch) {
            const location = {latitude: 32.109333, longitude: 34.855499}//tel aviv
            if('geolocation' in navigator) {
                const callback = (position: any) => { 
                    location.latitude = position.coords.latitude
                    location.longitude = position.coords.longitude
                    dispatch({type: 'FETCH_USER_LOCATION', location})
                  }
                const errorback = () => { 
                    dispatch({type: 'FETCH_USER_LOCATION', location})
                }
                navigator.geolocation.getCurrentPosition(callback, errorback);
            } else {
                dispatch({type: 'FETCH_USER_LOCATION', location})
            }
        }
    })
}

export default _locationServices