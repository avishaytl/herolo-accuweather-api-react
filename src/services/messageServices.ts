
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

interface messageServicesType{
    handleMsg: (msg: string, time: number, onEnd?: () => void) => void
}

function _messageServices():messageServicesType{
    return({
        handleMsg(msg, time, onEnd){ 
            if(onEnd)
                setTimeout(() => {
                    onEnd() 
                }, time + 1000)
            toast(msg, {
                position: "top-center",
                autoClose: time,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: false,
                draggable: false,
                progress: undefined,
            })
        }
    })
}

export default _messageServices