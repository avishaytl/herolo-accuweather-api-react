import _appServices from "./appServices"
import _locationServices from "./locationServices"
import _messageServices from "./messageServices"

export const appServices = _appServices()
export const locationServices = _locationServices()
export const messageService = _messageServices(); 