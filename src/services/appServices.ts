
interface appServicesType{
    getNameFromDateString: (dateString: string) => void
    callAPI: ({url}:{url: string, method?: string, body?: {}}) => Promise<null>
    getFirebaseJsonAsString: (data: {locationKey: string, isDayTime: boolean, city: string, metricC: number, imperialF: number, icon: number, iconPhrase: string, days: any[]}, isFavorites?: boolean, lastFields?: string) => string
    storage: { set: (key: string, value: string)=> void, get: (key: string)=> string}
    setThemeMode(theme: any): void
} 

function _appServices():appServicesType{
    return({
        setThemeMode(theme) {  
            const mode = this.storage.get('theme')
            if(mode)
                theme.setMode(mode)
        },
        storage: {
            set: (key, value) => {
                localStorage[key] = JSON.stringify(value);
            },
            get: (key) => {
                return localStorage[key] ? JSON.parse(localStorage[key]) : null;
            }
        },
        getFirebaseJsonAsString: ({locationKey, isDayTime, city, metricC, imperialF, icon, iconPhrase, days}, isFavorites, lastFields) => { 
            let currentDays = ``
            for(let i = 0; i < days.length;i++)
                currentDays += `"${i + 1}": {
                                    "mapValue": {
                                        "fields": {
                                            "Date":{
                                                "stringValue": "${days[i]['Date']}"
                                            },
                                            "DayIcon": { 
                                                "integerValue": "${days[i]['Day']['Icon']}"
                                            },
                                            "DayIconPhrase": { 
                                                "stringValue": "${days[i]['Day']['IconPhrase']}"
                                            },
                                            "NightIcon": { 
                                                "integerValue": "${days[i]['Night']['Icon']}"
                                            },
                                            "NightIconPhrase": { 
                                                "stringValue": "${days[i]['Night']['IconPhrase']}"
                                            },
                                            "Temperature": {
                                                "mapValue": {
                                                    "fields": {
                                                        "Maximum": {
                                                            "mapValue": {
                                                                "fields": {
                                                                    "Value": {
                                                                        "integerValue": "${parseInt(days[i]['Temperature']['Maximum']['Value']).toFixed(0)}"
                                                                    },
                                                                    "Unit": {
                                                                        "stringValue": "${days[i]['Temperature']['Maximum']['Unit']}"
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        "Minimum": {
                                                            "mapValue": {
                                                                "fields": {
                                                                    "Value": {
                                                                        "integerValue": "${parseInt(days[i]['Temperature']['Minimum']['Value']).toFixed(0)}"
                                                                    },
                                                                    "Unit": {
                                                                        "stringValue": "${days[i]['Temperature']['Minimum']['Unit']}"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },`
                                
            const raw = `{
                "fields": {
                    "LocationKey": {
                        "stringValue": "${locationKey}"
                    },
                    "isDayTime": {
                        "stringValue": "${isDayTime}"
                    },
                    "Date": {
                        "stringValue": "${new Date()}"
                    },
                    "City": {
                        "stringValue": "${city}"
                    },
                    "Icon": { 
                        "integerValue": "${icon}"
                    },
                    "IconPhrase": { 
                        "stringValue": "${iconPhrase}"
                    },
                    "Temperature": {
                        "mapValue": {
                            "fields": {
                                "MetricC": {
                                    "integerValue": "${parseInt(`${metricC}`).toFixed(0)}"
                                },
                                "ImperialF": {
                                    "integerValue": "${parseInt(`${imperialF}`).toFixed(0)}"
                                }
                            }
                        }
                    },
                    "Days": { 
                        "mapValue": {
                            "fields": {
                                ${currentDays}
                            }
                        }
                    }
                }
            }` 
            return isFavorites ? `{
                "fields": {
                    "${city}": {
                        "mapValue": 
                            ${raw}
                    }
                    ${lastFields ? (',' + lastFields.substring(lastFields.indexOf('{') + 1,lastFields.lastIndexOf('}') - 1) + '}') : ''}
                }
            }` : raw
        },
        getNameFromDateString: (dateString) => {
            let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            let d = new Date(dateString);
            let dayName = days[d.getDay()];
            return dayName
        },
        callAPI: async ({url, method, body}) => {
            let requestOptions: any = {
                method: method ? method : 'GET',
                redirect: 'follow',
                body: body
              }
            const response = await fetch(
                url,
                requestOptions
              );
            if(response){ 
                let json = await response.json() 
                return json
            }
            return null;
        }
    })
}

export default _appServices