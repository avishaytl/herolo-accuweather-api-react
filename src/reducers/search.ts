import { createSlice } from '@reduxjs/toolkit'

export interface CounterState {
  data: []
  offmode: boolean
}

const initialState: CounterState = {
  data: [],
  offmode: false
}

export const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
     setSearchData(state, action){ 
      state.data = action.payload.data
     },
     setOffmode(state, action){ 
      state.offmode = action.payload.offmode
     }
  },
})

export const { setSearchData, setOffmode } = searchSlice.actions

export default searchSlice.reducer