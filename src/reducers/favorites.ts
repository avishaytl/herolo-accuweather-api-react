import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface CounterState {
  data: [],
  favorite: string
}
 
const initialState: CounterState = {
  data: [],
  favorite: 'Tel Aviv'
}

export const favoritesSlice = createSlice({
  name: 'favorites',
  initialState,
  reducers: {
     setFavorites(state, action){
      state.data = action.payload.favorites
     },
     setCurrentFavorite(state, action){ 
      state.favorite = action.payload.favorite
     }
  },
})

// Action creators are generated for each case reducer function
export const { setFavorites, setCurrentFavorite } = favoritesSlice.actions

export default favoritesSlice.reducer