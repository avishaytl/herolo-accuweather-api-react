import { createSlice } from '@reduxjs/toolkit'

interface Day{
    Date: string
    Temperature: {Maximum: {Value: number, Unit: string}, Minimum: {Value: number, Unit: string}}
    Day: {Icon: number, IconPhrase: string}
    Night: {Icon: number, IconPhrase: string}
}

export interface LocationState {
    longitude: number
    latitude: number
    locationKey: string
    city: string
    weather: string
    weatherIcon: string
    isDayTime: boolean
    temperatureMetricC: number
    temperatureImperialF: number
    days: Day[]
    date: string
}

const initialState: LocationState = {
    longitude: 0, 
    latitude: 0,
    locationKey: '',
    city: '',
    weather: '',
    weatherIcon: '',
    isDayTime: true,
    temperatureMetricC: 0,
    temperatureImperialF: 0,
    days: [],
    date: ''
}

export const locationSlice = createSlice({
  name: 'location',
  initialState,
  reducers: { 
        setLocationPosition(state, action){
            state.longitude = action.payload.longitude
            state.latitude = action.payload.latitude
            state.locationKey = action.payload.locationKey
            if(state.city === '')
                state.city = action.payload.city
        },
        setLocationWeather(state, action){
            state.weather = action.payload.weather
            state.weatherIcon = `${action.payload.weatherIcon}`.length === 1 ? `0${action.payload.weatherIcon}` : action.payload.weatherIcon
            state.isDayTime = action.payload.isDayTime
            state.temperatureMetricC = action.payload.temperatureMetricC
            state.temperatureImperialF = action.payload.temperatureImperialF
            if(action.payload.city)
                state.city = action.payload.city
            if(action.payload.date)
                state.date = action.payload.date
        },
        setLocation5DaysWeather(state, action){
            state.days = action.payload.days
        }
  },
})

export const { setLocationPosition, setLocationWeather, setLocation5DaysWeather } = locationSlice.actions

export default locationSlice.reducer