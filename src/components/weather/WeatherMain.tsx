import React from "react"
import AddFavorite from "./AddFavorite"
import MainDaysWeekList from "./MainDaysWeekList"
import SelectedLocation from "./SelectedLocation"
import { MainView, TopBar, TopBarView } from "./_style"

const Weather = () =>{
    return(
        <MainView>   
            <TopBar>
                <TopBarView alignItems={'flex-start'}>
                    <SelectedLocation/>
                </TopBarView>
                <TopBarView alignItems={'flex-end'}>
                    <AddFavorite/>
                </TopBarView>
            </TopBar>
            <MainDaysWeekList/>
        </MainView>
    )
}

export default React.memo(Weather) 