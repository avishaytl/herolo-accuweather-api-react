import React from "react"
import { AiOutlineHeart } from "react-icons/ai"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../stores/_index"
import { AddFavoriteContainer, FavoriteIcon, FavoriteLabel } from "./_style"

const AddFavorite = () =>{
    const dispatch = useDispatch()
    const location = useSelector((state: RootState) => state.location)
    const isFavorite = useSelector((state: RootState) => state.favorites.favorite)
    const onClick = ()=>{
        const firebaseData = {
            isDayTime: location.isDayTime, city: location.city, metricC: location.temperatureMetricC, imperialF: location.temperatureImperialF, icon: location.weatherIcon, iconPhrase: location.weather
        }
        const days = location.days
        const locationKey = location.locationKey
        dispatch({type: 'FETCH_ADD_FAVORITE', firebaseData, days, locationKey})
    }
    return( 
        <AddFavoriteContainer isFavorite={location.city === isFavorite} onClick={onClick}>
            <FavoriteIcon>
                <AiOutlineHeart/> 
            </FavoriteIcon>
            <FavoriteLabel>
                {`Add To Favorites`}
            </FavoriteLabel>
        </AddFavoriteContainer> 
    )
}

export default React.memo(AddFavorite)