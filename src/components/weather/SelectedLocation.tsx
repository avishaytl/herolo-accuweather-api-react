import React from "react"
import { useCallback, useState } from "react"
import { useSelector } from "react-redux"
import { RootState } from "../../stores/_index"
import { Title } from "../../styles/_style"
import { SelectedLocationContainer, WeatherModeView } from "./_style"

const SelectedLocation = () =>{
    const city = useSelector((state: RootState) => state.location.city)
    const tempF = useSelector((state: RootState) => state.location.temperatureImperialF)
    const tempC = useSelector((state: RootState) => state.location.temperatureMetricC)
    const weatherIcon = useSelector((state: RootState) => state.location.weatherIcon)
    const weather = useSelector((state: RootState) => state.location.weather)
    const [temp,setTemp] = useState(true)
    const onClick = useCallback(()=>{
        setTemp(t => !t)
    },[])
    return( 
        <SelectedLocationContainer> 
            <WeatherModeView>
                <img alt="weatherIcon" src={`https://developer.accuweather.com/sites/default/files/${weatherIcon}-s.png`}/>
                {weather}
            </WeatherModeView>
            <Title pointer onClick={onClick}>
                {`${city}\n${temp ? tempC + '° C' : tempF + '° F'}`}
            </Title> 
        </SelectedLocationContainer> 
    )
}

export default React.memo(SelectedLocation)