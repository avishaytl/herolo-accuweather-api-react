import React from "react" 
import { useSelector } from "react-redux"
import { appServices } from "../../services/_index"
import { RootState } from "../../stores/_index"
import { Label, ListContainer, ListItem, Title } from "../../styles/_style"
const MainDaysWeekList = () =>{
    const days = useSelector((state: RootState) => state.location.days)
    const isDayTime = useSelector((state: RootState) => state.location.isDayTime)
    return(
        <ListContainer>
            {days.map((day)=>{
                const name = appServices.getNameFromDateString(day['Date'])
                const icon = `${day[isDayTime ? 'Day' : 'Night']['Icon']}`.length === 1 ? `0${day[isDayTime ? 'Day' : 'Night']['Icon']}` : day[isDayTime ? 'Day' : 'Night']['Icon']
                return(
                    <ListItem key={name + day['Date']}>
                        <Title>
                            {`${name}`}
                        </Title> 
                        <img alt="weatherIcon" src={`https://developer.accuweather.com/sites/default/files/${icon}-s.png`}/>
                        <Label>{`${day[isDayTime ? 'Day' : 'Night']['IconPhrase']}`}</Label>
                        <Title>
                            {`${day['Temperature']['Minimum']['Value']}°${day['Temperature']['Minimum']['Unit']} - ${day['Temperature']['Maximum']['Value']}°${day['Temperature']['Maximum']['Unit']}`}
                        </Title> 
                   </ListItem>)
            })}
        </ListContainer>
    )
}

export default React.memo(MainDaysWeekList)