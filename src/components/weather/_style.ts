import styled from "styled-components"
 
const MainView = styled.main`  
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    width: 95vw;
    max-width: 95vw;
    overflow-x: hidden;
    overflow-y: auto;
    max-height: 100%;
    @media (max-width: 500px) {
        flex-direction: column;
    }
` 
const TopBar = styled.div`  
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 1000px;
    max-width: 100%;
    @media (max-width: 500px) {
        flex-direction: column;
    }
` 
const TopBarView = styled.div<{alignItems: string}>`  
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    align-items: ${p => p.alignItems};
    justify-content: center;
    max-width: 50%;
    min-height: 200px;
` 
const AddFavoriteContainer = styled.div<{isFavorite: boolean}>`  
    display: flex;
    flex-direction: row;
    cursor: pointer;
    transition: all 0.5s ease;
    border-radius: 50px;
    padding: 0px 15px 0px 15px;
    background: ${p => p.isFavorite ? p.theme.paperNative : 'transparent'};
    color: ${p => p.isFavorite ? 'red' : p.theme.text};
    box-shadow: inset 0px 0px ${p => p.isFavorite ? '5px' : '0px'} 0px #9B9B9B;
    border: 4px solid ${p => p.isFavorite ? p.theme.border : 'transparent'};
    &:hover{
        background: ${p => p.theme.paperNative};
        border: 4px solid ${p => p.theme.border};
        color: red;
        box-shadow: inset 0px 0px 5px 0px #9B9B9B;
    }
` 
const FavoriteIcon = styled.div` 
    font-size: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
`
const FavoriteLabel = styled.h2`
    & :hover{ 
        border-bottom-color: ${p => p.theme.text};
    }
`
const SelectedLocationContainer = styled.div` 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    transition: all 0.5s ease;
    border: 4px solid transparent;
    color: ${p => p.theme.text};
    @media (max-width: 500px) {
        flex-direction: column;
    }
`
const WeatherModeView = styled.div` 
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transition: all 0.5s ease;
`
export {MainView, TopBar, TopBarView, AddFavoriteContainer, FavoriteIcon, FavoriteLabel, SelectedLocationContainer, WeatherModeView}