import styled from "styled-components"
 
const SearchBarContainer = styled.div` 
    width: 250px;
    max-width: 95vw;
    min-height: 70px;
    border: solid 2px ${p => p.theme.border}; 
    border-radius: 50px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    box-shadow: inset 0px 0px 5px 0px ${p => p.theme.paper};
    background: ${p => p.theme.background};
`
export {SearchBarContainer}