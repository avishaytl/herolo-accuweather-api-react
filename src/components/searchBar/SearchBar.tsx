/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useState } from "react"
import { SearchBarContainer } from "./_style"
import Select ,{StylesConfig}from 'react-select'
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../stores/_index"
import { Label } from "../../styles/_style"
import { messageService } from "../../services/_index"
import debounce from "lodash.debounce"

  const colourStyles: StylesConfig<any, true> = {
    control: (styles: any) => ({ ...styles, width: 200, flex: 1, borderWidth: 0,border: 'none', backgroundColor:'transparent' , boxShadow: 'none', }),
    option: (styles: any, { data, isDisabled, isFocused, isSelected }) => { 
      return {
        ...styles,
        borderWidth: 0,
        textAlign:'center',
        backgroundColor: isDisabled
          ? undefined
          : isSelected
          ? data.color
          : isFocused
          ? '#cecece'
          : undefined,
        color: isDisabled
          ? '#ccc'
          : isSelected
          ? true
            ? 'blue'
            : 'black'
          : data.color,
        cursor: isDisabled ? 'not-allowed' : 'default',
  
        ':active': {
            borderWidth: 0,
          ...styles[':active'],
          backgroundColor: !isDisabled
            ? isSelected
              ? 'blue'
              : '#cecece'
            : undefined,
        },
      };
    },
    multiValue: (styles, { data }) => { 
      return {
        ...styles,
        backgroundColor: 'green',
      };
    },
    multiValueLabel: (styles, { data }) => ({
      ...styles,
      color: data.color,
    }),
    multiValueRemove: (styles, { data }) => ({
      ...styles,
      color: data.color,
      ':hover': {
        backgroundColor: data.color,
        color: 'white',
      },
    }),
  }
  
const SearchBar = () =>{
    const dispatch = useDispatch()
    const data = useSelector((state: RootState) => state.search.data)
    const offmode = useSelector((state: RootState) => state.search.offmode)
    const date = useSelector((state: RootState) => state.location.date)
    const [selected,setSelected] = useState() 

    const onInputChange: any = useCallback((label: string) => { 
      if(label.indexOf(' ') === 0){

      }else if(/[^a-zA-Z\s]/i.test(label))
          messageService.handleMsg('String must includes only letters A-Z/a-z!',4000)
      else if(`${label}`.length > 3)
          dispatch({type: 'FETCH_SEARCH', label}) 
    },[])
    
    const debouncedChangeHandler = useCallback(debounce(onInputChange, 300), [])

    const onChange = useCallback((selectedOption) => {
        setSelected(selectedOption)
        const locationKey = selectedOption.locationKey
        const label = selectedOption.label
        console.log('selectedOption',label)
        dispatch({type: 'FETCH_LOCATION_WEATHER', locationKey, label}) 
    },[])
    return(
      <>
      {offmode && <Label>{`Last Firebase Update`}</Label>}
        <SearchBarContainer> 
             {!offmode ? <Select
                  placeholder={'Search'}
                  styles={colourStyles}
                  value={selected} 
                  onInputChange={debouncedChangeHandler}
                  onChange={onChange}
                  options={data}
              /> : <Label>{date}</Label>}
        </SearchBarContainer>
      </>
    )
}

export default React.memo(SearchBar)