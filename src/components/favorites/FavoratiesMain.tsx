
import { Label, ListContainer, ListItem, Title } from "../../styles/_style"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../stores/_index"
import React from "react"
import useHeaderSwitch from "../../hooks/useHeaderSwitch"

const tabs = [
    'Weather',
    'Favorites'
]

const FavoritesMain = () =>{
    const [switchPosition,actionSwitch] = useHeaderSwitch(tabs, 0)
    const favorites = useSelector((state: RootState) => state.favorites.data)
    const dispatch = useDispatch()
    return(
        <ListContainer>
            {favorites.map((fav)=>{
                const onClick = () =>{  
                    actionSwitch(0, 1)
                    dispatch({type: 'FETCH_CURRENT_FAVORITE', favorite: fav})
                }
                return(
                    <ListItem cursor onClick={onClick} key={fav['date']}>
                        <Title>
                            {`${fav['city']}`}
                        </Title> 
                        <img alt="weatherIcon" src={`https://developer.accuweather.com/sites/default/files/${fav['icon']}-s.png`}/>
                        <Label>{`${fav['iconPhrase']}`}</Label>
                        <Title>
                            {`${fav['temperatureMetricC']}° C / ${fav['temperatureImperialF']}° F`}
                        </Title> 
                        <Label>
                            {`${fav['date']}`}
                        </Label> 
                </ListItem>)
            })}
        </ListContainer>
    )
}

export default React.memo(FavoritesMain)