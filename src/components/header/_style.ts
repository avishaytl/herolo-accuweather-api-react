import styled from "styled-components"
 
const HeaderContainer = styled.header` 
    max-width: 95vw;
    min-width: 95vw;
    z-index: 2;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    min-height: 100px;
    -webkit-tap-highlight-color: transparent;
    @media (max-width: 500px) {
        flex-direction: column;
    }
`
const HeaderLabel = styled.p`
    font-size: 14px;
    top: 55px;
    color: ${p => p.theme.text};
    user-select: none;
    position: absolute;
    @media (max-width: 500px) {
        width: 100vw;
        text-align: center;
        
    }
    & a{
        font-weight: bold;
        text-decoration: none;
        color: ${p => p.theme.text};
        transition: border-width 0.5s ease;
        border: solid 2px transparent;
        transition: all 0.5s ease;
    }
    & a:hover{ 
        border-bottom-color: ${p => p.theme.text};
    }
`
const HeaderSwitchContainer = styled.div` 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
    @media (max-width: 500px) {
        justify-content: center;
        
    }
`
const HeaderSwitchChild = styled.p`
    color: ${p => p.theme.text};
    text-align: center;
    cursor: pointer; 
    padding: 10px 0px 10px 0px;
    user-select: none;
    margin: 0px 5px 0px 5px;
    transition: all 0.5s ease;
`
const ThemeIcon = styled.div`
    color: ${p => p.theme.text};
    transition: all 1s ease;
    cursor: pointer;
    padding: 10px;
    font-size: 30px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`
const ImageView = styled.div`
    max-width: 250px;
    overflow: hidden;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    @media (max-width: 500px) {
        
    }
`
const LogoImage = styled.img`
    max-width: 70%;
`
const CreateByLabel = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`
export {HeaderContainer, HeaderLabel, HeaderSwitchContainer, HeaderSwitchChild, ThemeIcon, ImageView, CreateByLabel, LogoImage}