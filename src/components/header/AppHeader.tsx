import React from "react"
import HeaderSwitch from "./HeaderSwitch"
import { HeaderContainer, HeaderLabel, ImageView, LogoImage } from "./_style"

const AppHeader = () =>{
    return(
        <HeaderContainer>
            <ImageView>
                <LogoImage src={require('../../assets/images/newLogo.png')} alt={'logo'}/> 
                <HeaderLabel>Weather Task By ▸ &nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.linkedin.com/in/avishay-tal-8559491a5/">Avishay Tal</a></HeaderLabel>
            </ImageView>
            <HeaderSwitch/>
        </HeaderContainer>
    )
}

export default React.memo(AppHeader)