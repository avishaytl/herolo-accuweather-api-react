import useHeaderSwitch from "../../hooks/useHeaderSwitch"
import { HeaderSwitchContainer, HeaderSwitchChild, ThemeIcon } from "./_style"
import { AiOutlineBgColors } from 'react-icons/ai'
import { useTheme } from "../../themes/_index"
import { appServices } from "../../services/_index"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../stores/_index"
import React from "react"

const tabs = [
    'Weather',
    'Favorites'
]

const HeaderSwitch = () =>{
    const [switchPosition,actionSwitch] = useHeaderSwitch(tabs, 0)
    const theme = useTheme()
    const dispatch = useDispatch()
    const city = useSelector((state: RootState) => state.location.city)
    const onActionTheme = ()=>{
        appServices.storage.set('theme', theme.mode === 'light' ? 'dark' : 'light')
        theme.setMode(theme.mode === 'light' ? 'dark' : 'light')
    }
    return(
        <HeaderSwitchContainer> 
            {tabs.map((label, index)=>{
                const onClick = ()=>{
                    actionSwitch(index, switchPosition)
                    if(label === 'Favorites')
                        dispatch({type: 'FETCH_FAVORITES', city: city})
                }
                return(<HeaderSwitchChild className={`${!index ? 'switch-visible' : 'switch-hidden'}`} key={label} onClick={onClick} id={`${label.toLowerCase()}-switch`}>{label}</HeaderSwitchChild>)
            })} 
            <ThemeIcon>
                <AiOutlineBgColors onClick={onActionTheme}/> 
            </ThemeIcon>
        </HeaderSwitchContainer>
    )
}

export default React.memo(HeaderSwitch)