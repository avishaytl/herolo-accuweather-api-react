import FavoritesMain from "../components/favorites/FavoratiesMain"
import { PageContainer } from "../styles/_style"

const Favorites = () =>{
    return(
        <PageContainer className={'page-hidden'} id={'favorites-page'}>
            <FavoritesMain/>
        </PageContainer>
    )
}

export default Favorites