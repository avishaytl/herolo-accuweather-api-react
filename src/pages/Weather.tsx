import SearchBar from "../components/searchBar/SearchBar"
import WeatherMain from "../components/weather/WeatherMain"
import { PageContainer } from "../styles/_style"

const Weather = () =>{
    return(
        <PageContainer className={'page-visible'} id={'weather-page'}>  
            <SearchBar/>
            <WeatherMain/>
        </PageContainer>
    )
}

export default Weather