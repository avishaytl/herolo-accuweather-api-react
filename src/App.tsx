import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import AppHeader from './components/header/AppHeader'
import Favorites from './pages/Favorites'
import Weather from './pages/Weather'
import { appServices, locationServices } from './services/_index'
import { ToastContainer } from 'react-toastify'
import { AppContainer, BackgroundView } from './styles/_style'
import { useTheme } from './themes/_index'

function App() {
  const dispatch = useDispatch() 
  const theme = useTheme()
  useEffect(()=>{
    locationServices.getCurrentPosition(dispatch)
    appServices.setThemeMode(theme)
  },[])
  return ( 
    <AppContainer>
      {/* <BackgroundView/> */}
      <AppHeader/>
      <Weather/>
      <Favorites/>
      <ToastContainer/>
    </AppContainer>
  )
}

export default App
