import { createGlobalStyle } from 'styled-components'
 
const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    overflow: hidden;
    max-width: 100vw;
    max-height: 100vh;
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    border-box: box-sizing; 
    user-select: none; 
  } 
  ::-webkit-scrollbar {
    width: 0px;
  }
 
  ::-webkit-scrollbar-track {
    display: hidden;
  }
 
  ::-webkit-scrollbar-thumb {
    display: hidden;
  }
 
  ::-webkit-scrollbar-thumb:hover {
    display: hidden;
  }
`
export {GlobalStyle}