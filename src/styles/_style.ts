import styled from "styled-components"

const PageContainer = styled.div<{background?: string}>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    flex: 1;
    min-width: 100%;
    z-index: 2;
`

const AppContainer = styled.div`
    background: ${p => p.theme.background};
    flex: 1;
    min-width: 100vw;
    min-height: 100vh;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`
const BackgroundView = styled.div`
    background: ${p => p.theme.paper};
    min-width: 100vw;
    min-height: 90vh;
    border-radius: 50%;
    z-index: 0;
    top:0;
    position: absolute;
`

const Title = styled.span<{pointer?: boolean}>`
    font-size: 24px;
    white-space: pre-line;
    padding-left: 10px;
    cursor: ${p => p.pointer ? 'pointer' : 'auto'};
    text-align: center;
`
const Label = styled.span<{pointer?: boolean}>`
    font-size: 14px;
    white-space: pre-line; 
    color: ${p => p.theme.text};
    text-align: center;
    max-width: 140px;
`
const ListContainer = styled.div`
    flex: 1;
    min-height: 350px;
    max-width: 80vw;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    align-items: flex-start;
    overflow-x: auto;
    overflow-y: hidden;
    @media (max-width: 500px) {
        max-width: 95vw;
    }
`
const ListItem = styled.div<{cursor?: boolean}>`
    min-height: 250px;
    min-width: 250px;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 0px 5px 0px 5px;
    overflow: hidden;
    border: solid 2px ${p => p.theme.border};
    color: ${p => p.theme.text};
    background: ${p => p.theme.background};
    box-shadow: inset 0px 0px 5px 0px ${p => p.theme.paper};
    cursor: ${p => p.cursor ? 'pointer' : 'auto'};
`
export {AppContainer, PageContainer, BackgroundView, Title, Label, ListContainer, ListItem}