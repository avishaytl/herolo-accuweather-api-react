import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import store from './stores/_index'
import { Provider } from 'react-redux'
import {GlobalStyle} from './styles/globalStyle'
import ThemeManager from './themes/_index'

ReactDOM.render(
  <ThemeManager>
    <Provider store={store}>
      <GlobalStyle/>
      <App/>
    </Provider> 
  </ThemeManager>,
  document.getElementById('root')
)